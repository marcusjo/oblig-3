package edu.ntnu.idatt2001.oblig3;

import java.util.ArrayList;
import java.util.List;

import edu.ntnu.idatt2001.oblig3.game.DeckOfCards;
import edu.ntnu.idatt2001.oblig3.game.PlayingCard;
import edu.ntnu.idatt2001.oblig3.game.PlayingHand;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

/**
 * @author marcusjohannessen
 */

public class Controller {

    private DeckOfCards deckOfCards;
    private PlayingHand playingHand;
    private List<PlayingCard> dealtHand;

    /**
     * Constructor
     */
    public Controller() {
        this.deckOfCards = new DeckOfCards();
        this.playingHand = new PlayingHand();
        this.dealtHand = new ArrayList<>();
    }

    @FXML
    private TextField amountField;
    @FXML
    private Button dealHandButton;
    @FXML
    private Button checkHandButton;
    @FXML
    private TextField sumOfFaces;
    @FXML
    private TextField cardsOfHearts;
    @FXML
    private TextField flush;
    @FXML
    private TextField queenOfSpades;
    @FXML
    private TextField outPutArea;


    /**
     * If amount field is empty, deal hand button is disabled
     */
    @FXML
    public void handleKeyReleasedDealButton(){
        String amountText = amountField.getText();
        boolean disableButton = (amountText.isEmpty() || amountText.trim().isEmpty());
        dealHandButton.setDisable(disableButton);
        //Set false because it can be true from handleDealHandButton()
        checkHandButton.setDisable(false);
    }

    /**
     *
     * @param e
     * @throws IllegalArgumentException
     * if deal hand button is pressed, a hand between 5 and 8 is given
     */
    @FXML
    public void handleDealHandButton(ActionEvent e) throws IllegalArgumentException{
        if(e.getSource().equals(dealHandButton)){
            try{
                String amountText = amountField.getText();
                int amount = Integer.parseInt(amountText);
                this.dealtHand = deckOfCards.dealHand(amount);
                outPutArea.setText("Hand is dealt, check if you want!");
            }catch (IllegalArgumentException ex){
                ex.printStackTrace();
                outPutArea.setText("Hand have to be between 5 and 8");
                checkHandButton.setDisable(true);
            }
            amountField.clear();
            dealHandButton.setDisable(true);
            sumOfFaces.clear();
            cardsOfHearts.clear();
            flush.clear();
            queenOfSpades.clear();
        }
    }

    /**
     *
     * @param e
     * calls all methods that gives output to text fields
     */
    @FXML
    public void handleCheckHandButton(ActionEvent e){
        if(e.getSource().equals(checkHandButton)){
            handleSumOfFaces();
            handleCardsOfHearts();
            handleFlush();
            handleOutPutArea();
            handleQueenOfSpades();
        }
    }

    /**
     * Shows sum of faces in text field
     */
    @FXML
    private void handleSumOfFaces(){
        int sumOfFacesInt = playingHand.sumOfFaces(dealtHand);
        String text = Integer.toString(sumOfFacesInt);
        sumOfFaces.setText(text);
    }

    /**
     * Shows all cards of type hearts in text field
     */
    @FXML
    private void handleCardsOfHearts(){
        if(playingHand.getHearts(dealtHand).isEmpty()){
            cardsOfHearts.setText("No hearts");
        }else{
            List<PlayingCard> hearts = playingHand.getHearts(dealtHand);
            cardsOfHearts.setText((hearts.toString()).replaceAll("[<>\\[\\],-]",""));
        }
    }

    /**
     * Yes if text field is five card flush, no if not
     */
    @FXML
    private void handleFlush(){
        String isFlush = playingHand.isFlush(dealtHand)? "Yes":"No";
        flush.setText(isFlush);
    }

    /**
     * Tells if queen of spade is occurring in dealt hand
     */
    @FXML
    private void handleQueenOfSpades(){
        String isOccurring = playingHand.queenOfSpade(dealtHand)? "Is occurring!": "Not here";
        queenOfSpades.setText(isOccurring);
    }

    /**
     * Shows the dealt hand in the text area
     */
    @FXML
    private void handleOutPutArea(){
        outPutArea.setText(dealtHand.toString().replaceAll("[<>\\[\\],-]",""));
    }
}