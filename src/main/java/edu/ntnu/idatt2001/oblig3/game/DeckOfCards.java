package edu.ntnu.idatt2001.oblig3.game;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;


/**
 * @author marcusjohannessen
 */

public class DeckOfCards {
    private final char[] suit = {'S', 'H', 'D', 'C'};
    private List<PlayingCard> cards = new ArrayList<>(52);
    private List<PlayingCard> dealtHand;

    /**
     * Constructor
     * generates 52 cards type PlayingCard
     * in a collection of cards
     */
    public DeckOfCards() {
        for (char card : suit) {
            for (int i = 1; i <= 13; i++) {
                this.cards.add(new PlayingCard(card, i));
            }
        }
        this.dealtHand = new ArrayList<>();
    }

    /**
     * @return List of 52 cards type PlayingCard
     */

    public List<PlayingCard> getCards() {
        return cards;
    }

    /**
     * @param amount
     * @return amount of random cards from the deck between 5 and 8
     */
    public List<PlayingCard> dealHand(int amount) {

        if (amount < 5 || amount > 8) {
            throw new IllegalArgumentException("Have to be between 5 and 8");
        }
        Random random = new Random();
        this.dealtHand = random.ints(amount, 0, cards.size()).mapToObj(
                i -> cards.get(i)).collect(Collectors.toList());

        return this.dealtHand;
    }
}