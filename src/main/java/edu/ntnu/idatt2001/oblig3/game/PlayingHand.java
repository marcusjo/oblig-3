package edu.ntnu.idatt2001.oblig3.game;

import edu.ntnu.idatt2001.oblig3.game.PlayingCard;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * @author marcusjohannessen
 * a Class that represent a players hand
 */

public class PlayingHand {

    public PlayingHand() {
    }

    /**
     *
     * @param playingCards
     * @return true if player have a five card flush
     *
     */
    public boolean isFlush(List<PlayingCard> playingCards){
        List<Character> suits = new ArrayList<>();
        playingCards.forEach(card -> suits.add(card.getSuit())); //List of just suits

        //Gives a map of key: suit, and value: amount of suit
        Map<Character, Long> count = suits.stream()
                .collect(Collectors.groupingBy(suit -> suit, Collectors.counting()));

        long amount = count.entrySet().stream().max(Map.Entry.comparingByValue()).get().getValue();
        return (amount >= 5);
    }

    /**
     *
     * @param playingCards
     * @return A list of only card type hearts from a list of cards
     * if list is empty the return null
     */
    public List<PlayingCard> getHearts(List<PlayingCard> playingCards){
        Stream<PlayingCard> streamOfHearts = playingCards
                .stream()
                .filter(card -> card.getSuit()== 'H');
        return streamOfHearts.collect(Collectors.toList());
    }

    /**
     *
     * @param playingCards
     * @return the sum of faces in a given list
     */
    public int sumOfFaces(List<PlayingCard> playingCards){

        int sum = 0;
        for(PlayingCard card: playingCards){
            sum += card.getFace();
        }
        return sum;
    }

    /**
     *
     * @param playingCards
     * @return true if queen of spade exists in list, false if not.
     */
    public boolean queenOfSpade(List<PlayingCard> playingCards){
        return playingCards.stream().anyMatch(card -> card.getAsString()
                .equals("S11"));
    }
}