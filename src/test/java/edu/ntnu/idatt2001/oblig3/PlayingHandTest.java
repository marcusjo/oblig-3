package edu.ntnu.idatt2001.oblig3;

import edu.ntnu.idatt2001.oblig3.game.PlayingCard;
import edu.ntnu.idatt2001.oblig3.game.PlayingHand;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class PlayingHandTest {

    public PlayingHand playingHand;

    public List<PlayingCard> playingCards;

    public PlayingHandTest() {
        this.playingHand = new PlayingHand();
        this.playingCards = new ArrayList<>();
        //Does not make sense to check for other letters and
        //numbers outside of 1-13 because deck of cards is created in constructor
        //This is added to see if the methods pass given tests
        playingCards.add(new PlayingCard('S',11)); //for queen of spade test
        playingCards.add(new PlayingCard('H',1)); //for getHearts test
        playingCards.add(new PlayingCard('H',2)); //and flush test
        playingCards.add(new PlayingCard('H',3));//
        playingCards.add(new PlayingCard('H',4));//
        playingCards.add(new PlayingCard('H',5));//
        playingCards.add(new PlayingCard('D',9));
        playingCards.add(new PlayingCard('C',5));
    }


    @Test
    @DisplayName("Assert that 5 cards of suit hearts gives flush")
    void isFlush() {
        Assertions.assertTrue(playingHand.isFlush(playingCards));
    }

    @Test
    void getHearts() {
        Assertions.assertEquals(playingHand.getHearts(playingCards).toString(),
                //Expect just hearts, (Weird format)
                "[H1 , H2 , H3 , H4 , H5 ]");
    }
    @Test
    @DisplayName("Sum of faces equals 40")
    void sumOfFaces() {
        Assertions.assertEquals(playingHand.sumOfFaces(playingCards), 40);
    }

    @Test
    void queenOfSpade() {
        Assertions.assertTrue(playingHand.queenOfSpade(playingCards));
    }

}